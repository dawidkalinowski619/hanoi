import time
import os

print("Witaj w grze \"Wieże Hanoi\"! Gra polega na tym, aby przenieść wszystkie krążki ze słupka A na dowolny inny, "
      "przy czym nieodzwolone jest umieszczenie większego krążka na mniejszym.")
if not os.path.exists("settings.txt") or os.stat("settings.txt").st_size == 0:
    file = open("settings.txt", "w")
    file.write(str(1))
file = open("settings.txt", "r")
t = file.read()


def drukarnia():
    for i in ('A', 'B', 'C'):
        print(i + " " * (n * 2 - 1), end="")
    print()
    for i in range(n - 1, -1, -1):
        for j in range(0, 3):
            if len(towers[j]) > i and towers[j][i]:
                amount_of_underlines = towers[j][i]
                character = "_" * amount_of_underlines
            else:
                amount_of_underlines = 1
                character = "|"
            print(character + " " * (n * 2 - amount_of_underlines), end="")
        print()


while True:
    print(
        "Wpisz /play aby rozpocząć\nWpisz /auto aby zobaczyć automatyczne rozwiązanie\nWpisz /settings aby "
        "przejść do ustawień\nTeraz, jak i w dowolnym momencie gry możesz wpisać /exit aby wyjść")
    choice = input()
    if choice.lower() == "/play":
        towers = [[], [], []]
        while True:
            n = int(input("Podaj ilość krążków: "))
            if n <= 0:
                print("Nieprawidłowa wartość! ", end='')
            else:
                break
        for i in range(n, 0, -1):
            towers[0].append(i)
        moves_count = 0
        print("\nAby przenieść krążek podaj literę słupka początkowego i docelowego, np: AB, AC, BA itd.")
        print("\nAktualny stan słupków:\n")
        drukarnia()
        print("\nIlość ruchów:", moves_count)

        while len(towers[1]) < n and len(towers[2]) < n:
            current_move = input("\nPodaj swój ruch: ")
            if current_move.lower() == "/exit":
                break
            elif len(current_move) != 2:
                print("Podaj słupek początkowy i docelowy, np: AB, AC, BA itd.")
                continue

            source_tower = current_move[0]
            target_tower = current_move[1]
            if source_tower == target_tower:
                print("Dlaczego chcesz zdjąć krążek ze słupka i z powrotem go tam założyć?")
                continue
            if source_tower == "A" or source_tower == "a":
                source_tower = towers[0]
            elif source_tower == "B" or source_tower == "b":
                source_tower = towers[1]
            elif source_tower == "C" or source_tower == "c":
                source_tower = towers[2]
            else:
                print("Nieprawidłowy słupek początkowy. Spróbuj ponownie.")
                continue
            if target_tower == "A" or target_tower == "a":
                target_tower = towers[0]
            elif target_tower == "B" or target_tower == "b":
                target_tower = towers[1]
            elif target_tower == "C" or target_tower == "c":
                target_tower = towers[2]
            else:
                print("Nieprawidłowy słupek docelowy. Spróbuj ponownie.")
                continue
            if source_tower and target_tower:
                if source_tower[-1] > target_tower[-1]:
                    print("Nie można umieścić większego krążka na mniejszym. Spróbuj ponownie.")
                    continue
            if not source_tower:
                print("Wybrany słupek jest pusty.")
                continue
            current_disk = source_tower.pop()
            target_tower.append(current_disk)
            moves_count += 1
            print("\nAktualny stan słupków:\n")
            drukarnia()
            print("\nIlość ruchów:", moves_count)
        if len(towers[1]) == n or len(towers[2]) == n:
            if moves_count == (2 ** n - 1):
                print("Gratulacje! Udało ci się ukończyć grę w", moves_count, "ruchach, co jest najmniejszą możliwą "
                                                                              "liczbą ruchów dla", n, "krążków.\n")
            else:
                print("Gratulacje! Udało ci się ukończyć grę w", moves_count, "ruchach.\n")
    if choice.lower() == "/auto":
        while True:
            n = int(input("Podaj ilość krążków: "))
            if n <= 0:
                print("Nieprawidłowa wartość! ", end='')
            else:
                break
        moves = []


        def TowerOfHanoi(n, source, destination, auxiliary):
            if n == 1:
                moves.append(str(source + str(destination)))
                return
            TowerOfHanoi(n - 1, source, auxiliary, destination)
            moves.append(str(source + str(destination)))

            TowerOfHanoi(n - 1, auxiliary, destination, source)


        TowerOfHanoi(n, 'A', 'B', 'C')
        all_used_moves = moves.copy()
        towers = [[], [], []]

        for i in range(n, 0, -1):
            towers[0].append(i)
        moves_count = 0

        while len(towers[1]) < n:
            current_move = moves.pop(0)
            source_tower = current_move[0]
            target_tower = current_move[1]
            if source_tower == "A":
                source_tower = towers[0]
            elif source_tower == "B":
                source_tower = towers[1]
            elif source_tower == "C":
                source_tower = towers[2]
            if target_tower == "A":
                target_tower = towers[0]
            elif target_tower == "B":
                target_tower = towers[1]
            elif target_tower == "C":
                target_tower = towers[2]
            current_disk = source_tower.pop()
            target_tower.append(current_disk)
            moves_count += 1
            print("\n")
            drukarnia()
            print("Krok " + str(moves_count) + " (" + str(current_move) + ")")
            time.sleep(float(t))

        print("\nNajmniejsza możliwa ilość ruchów dla " + str(n) + " krążków to " + str(moves_count) +
              ".\nDla n krążków najmniejszą możliwą ilość ruchów opisuje się wzorem 2^n - 1.\nWszystkie kroki "
              "po kolei: ", end="")
        print(all_used_moves, "\n")
    if choice.lower() == "/exit":
        break
    if choice.lower() == "/settings":
        while True:
            print("Aktualny odstęp czasowy: " + str(t) + "s")
            t = (input("Podaj odstęp wyświetlania kolejnych kroków w trybie auto "
                       "(0 = natychmiast) (Wpisz /cancel, aby anulować): "))
            if t.lower() == "/cancel":
                break
            if float(t) < 0:
                print("Nieprawidłowa wartość! ", end='')
            else:
                break
        file = open("settings.txt", "w")
        file.write(str(t))
        print("Zmiany zostały zapisane.\n")

# 1. Stworzenie menu gry z co najmniej dwoma opcjami: graj i wyjdź.    TAK
#
#
# 2. Zaprojektowanie i zaprogramowanie struktury słupków i dysków      TAK
#    przy pomocy słowników lub/i list. Zarówno słupki jak i dyski
#    mają być odwzorowane graficznie w konsoli i uaktualniane po
#    każdym ruchu.
#
#
# 3. Zaprogramowanie funkcji zmieniającej położenie dysku. Gra         TAK
#    pyta gracza o kolejny ruch i go wykonuje.
#
#
# 4. Umożliwienie graczowi zakończenia gry po każdym ruchu.            TAK
#
#
# 5. Wyświetlanie licznika pokazującego ilość ruchów od początku       TAK
#    gry.
#
#
# 6. Zastosowanie mechanizmu blokowania nieprawidłowego ruchu.         TAK
#    Gracz nie może umieścić większego dysku na mniejszym.
#    W takim przypadku wyświetlany jest odpowiedni komunikat,
#    a gracz ma możliwość kolejnego ruchu.
#
#
# 7. Cały skrypt działa dla dowolnej liczby dysków.                    TAK
#
#
# 8. Zaprogramowanie automatycznego rozwiązania dla problemu wież      TAK, Algorytm został zaczerpnięty z
#    hanoi (dla 4 lub więcej dysków) i dodanie takiej opcji do         https://www.geeksforgeeks.org/python-program-for-tower-of-hanoi/
#    menu. Na ekranie sa rysowane kolejne kroki wg algorytmu
#    rekurencyjnego rozwiązującego  problem wież Hanoi. Rysowanie
#    powinno być identyczne jak dla zwykłego gracza.
